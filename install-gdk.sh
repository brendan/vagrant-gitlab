echo "Installing GDK"

sudov apt-get update

sudo apt-get install -y curl openssh-server ca-certificates 
sudo apt-get install -y gnupg2 build-essential
sudo apt-get install -y libssl-dev libreadline-dev zlib1g-dev

## GIT

# Add apt-add-repository helper script
sudo apt-get install software-properties-common python-software-properties
# This PPA contains an up-to-date version of Go
sudo add-apt-repository ppa:longsleep/golang-backports
# This PPA contains an up-to-date version of git
sudo add-apt-repository ppa:git-core/ppa
sudo apt-get update
sudo apt-get install git postgresql postgresql-contrib libpq-dev redis-server libicu-dev cmake g++ libre2-dev libkrb5-dev libsqlite3-dev golang-1.10-go ed pkg-config graphicsmagick

git --version

## Node

sudo apt-get install -y nodejs npm
nodejs --version

## Yarn

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt-get update && sudo apt-get install yarn

## Go

snap install go --classic
