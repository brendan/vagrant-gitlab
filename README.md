# Vagrant container for Gitlab development

## Getting Started

### Mac

* Install [Vagrant](https://www.vagrantup.com/downloads.html)
* Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* `vagrant plugin update`
* `vagrant box update`
